Master thesis, fall 2019: Exploring host-microbiome interactions in Norwegian Salmon via weighted network analysis
==================================================================================================================

Most of the code is written into R-markdown documents so that the
results of the analyses are reproducible.

-   The branch master\_thesis\_code kept as documentation for the master
    thesis

<img src="images/other/Method.png" alt="General pipeline. Numbers represent the steps in the pipeline. 1: Preparation of data, 2: Normalization and transformation, 3: Network creation and module detection, 4: Processing of non-omics data, 5: Visualization of correlations between eigennodes and sample traits, 6: Detailed exploration of selected modules." width="100%" />
<p class="caption">
General pipeline. Numbers represent the steps in the pipeline. 1:
Preparation of data, 2: Normalization and transformation, 3: Network
creation and module detection, 4: Processing of non-omics data, 5:
Visualization of correlations between eigennodes and sample traits, 6:
Detailed exploration of selected modules.
</p>

Abstract
--------

Invisible to the naked eye, but present everywhere. Microbial organisms
live and reproduce in almost any conceivable environment on the planet.
In recent years, researchers have started to gain quantified information
of the microscopic world, fueled by the technical advances in sequencing
technology. From the presence of microbes in environmental samples
(metagenomics) to the processes within a cell (transcriptomics), omics
data has revolutionized our understanding of the microcosmos.

One of the places that microbial communities inhabit is the intestines
of animals. Many studies have shown that host-associated microbes have a
crucial role in the development of the host and are vital for critical
functions within many animals. Many studies focus their attention on the
microbiota of mammals, and there has been less focus on animals in
aquatic environments.

The farmed Atlantic salmon is of great importance for the Norwegian
economy. The Atlantic salmon is also an exciting study object due to the
dramatic environmental change the fish experiences during its lifetime.
The transition from freshwater to saltwater would kill most other fish
and is an extreme barrier for the proliferation of the salmon’s gut
microbiota.

In this thesis, we developed a pipeline using the R-package
weighted-correlation-network-analysis (WGCNA) to create networks and
detect modules in data from a long-term feeding trial of farmed Atlantic
salmon. Through the use of representative profiles for each module, this
network-based dimensionality reduction approach gives a holistic
approach to the discovery of potential host symbionts.

Complex behaviors emerge from the pairwise interactions of individual
objects on every level (genes, proteins, metabolites, cells, animals).
Network analysis differs from traditional methods of data analysis by
analyzing all relations between objects and studying how they behave
together.

In this thesis, we analyze two datasets: one of the host gene expression
(RNAseq) and another for the microbial abundance (16S rRNA amplicon
sequencing), both of which were previously analyzed separately: [Rudi et
al. 2018](https://www.doi.org/10.1128/AEM.01974-17), [Gillard et al.
2017](https://www.doi.org/10.1111/mec.14533). We found confirmation of
the central status of several bacteria as potential host-symbionts but
was limited in our ability to link these bacteria to specific sets of
genes by the strong effect of the freshwater-saltwater transition.

A method for regressing out principal components was used to remove
large effects in the data in the hopes that it would reveal the subtle
interactions. Although the method was successful in discovering genes
involved in lipid metabolism, the analysis showed no positive
correlation between any group of genes and microorganisms. This lack of
positive relationship goes against the established literature on the
subject; however, there are some limitations of the method. We discuss
these and other challenges and potential improvements to the pipeline
for future studies.

<img src="images/other/OTU_network_all_data.png" width="100%" />
