#!/bin/bash
#SBATCH --ntasks=1                    # same as (-n 1) number of CPUs
#SBATCH --nodes=1                     # same as (-N 1), number of nodes
#SBATCH --job-name=retrieveData        # same as (-J testscript), name for the job(hashtagSBATCH --mem=18G# Default memory per CPU is 3GB.)
#SBATCH --partition=smallmem          # verysamellmem < 10 GB RAM, smallmem 10-150GB RAM


module purge
module load anaconda3 # has pandoc installed
module load R/3.5.0 # Correct R version
module list

Rscript -e "Sys.setenv(RSTUDIO_PANDOC='/usr/lib/rstudio-server/bin/pandoc'); rmarkdown::render('step01_RNA_retrieveData.Rmd', output_file='step01_RNA_retrieveData.html')"
