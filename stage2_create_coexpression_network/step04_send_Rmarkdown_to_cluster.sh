#!/bin/bash
#SBATCH --ntasks=1               # same as (-n 1) number of CPUs
#SBATCH --nodes=1                # same as (-N 1), number of nodes
#SBATCH --job-name="name_of_job" # same as (-J testscript), name for the job
#SBATCH --partition=smallmem     # verysamellmem < 10 GB RAM, smallmem 10-150GB RAM
#SBATCH --mem=4G


module purge
module load anaconda3 # Has pandoc installed
module load R/3.5.0 # Correct R version
module list

Rscript -e "Sys.setenv(RSTUDIO_PANDOC='/usr/lib/rstudio-server/bin/pandoc'); \
rmarkdown::render('rna_makeModules_template.Rmd', \
output_file='rna_makeModules_template.html')"
