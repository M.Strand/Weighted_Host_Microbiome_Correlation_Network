Check_ME_corr_and_overlap <- function(set_A, set_B, name_set_A = "set_A", name_set_B = "set_B", plot_true = T){
  # This function expects the color vector of the WGCNA output to have gene names
  # Using plot_true = TRUE requires pheatmap and cowplot
  
  # Split genes into a list based on their module number ----
  split_gene_module <- function(x){
    x <- x$colors
    x.s <- list()
    for(m in unique(x)){
      x.s[[paste0("ME",m)]] <- names(x)[which(x == m)]
    }
    x.s
  }
  
  # Apply function
  split_set_A <- split_gene_module(set_A)
  split_set_B <- split_gene_module(set_B)
  
  
  # Find fraction of identical genes between set modules. ----
  overlap_sets <- function(x,y){
    input <- list(x,y)
    univ <- unique(c(x, y))
    VD <- gplots::venn(input, universe = univ, show.plot=FALSE, simplify=TRUE, intersections=FALSE)
    attr(VD, "class") <- NULL
    VD <-  as.data.frame(VD)
    VD$frac_total <- VD$num/sum(VD$num)
    VD$frac_min <-  VD$num/min(VD$num[c(2,3)])
    
    # In the matrix each set is called A and B
    idx <- which((VD$A == 1) & (VD$B == 1)) # This will always be four, but we test just to be sure.
    VD.score <- VD$frac_min[idx]
    VD.score # fraction similarity
  }
  
  
  # Apply function, calculating overlap.
  # First create an empty matrix, because we are going to do this for every pair of between set modules.
  overlap_matrix <- matrix(nrow = length(split_set_A), 
                           ncol = length(split_set_B), 
                           dimnames = list(names(split_set_A), 
                                           names(split_set_B)))
  for(ma in seq_along(split_set_A)){
    for(mb in seq_along(split_set_B)){
      overlap_matrix[ma, mb] <- overlap_sets(split_set_A[[ma]],split_set_B[[mb]])
    }
  }
  # reorder after ME number
  rownames(overlap_matrix) %>% sub("ME","", .) %>% as.numeric() %>% order() -> idx.r
  colnames(overlap_matrix) %>% sub("ME","", .) %>% as.numeric() %>% order() -> idx.c
  overlap_matrix <- overlap_matrix[idx.r, idx.c]
  
  
  # Calculate correlation ----
  correlation_matrix <- WGCNA::bicor(set_A$MEs, set_B$MEs)
  # reorder after ME number
  rownames(correlation_matrix) %>% sub("ME","", .) %>% as.numeric() %>% order() -> idx.r
  colnames(correlation_matrix) %>% sub("ME","", .) %>% as.numeric() %>% order() -> idx.c
  correlation_matrix <- correlation_matrix[idx.r, idx.c]
  
  
  # Make an enpty matrix to selection in as text.
  select_matrix <- matrix("", 
                          nrow = nrow(correlation_matrix), ncol = ncol(correlation_matrix), 
                          dimnames = list(rownames(correlation_matrix),colnames(correlation_matrix)))
  
  # Which element has the highest value? 
  # The one with the lowest index is chosen if there is a tie
  # The chance for a tie is rather small as we are dealing with continuous values.
  
  
  # Find the highest correlation for each row.
  for(r in 1:nrow(select_matrix)){
    idx <- base::which.max(correlation_matrix[r,])
    select_matrix[r, idx] <- paste0(select_matrix[r, idx], "r")
  }
  
  # Find the highest correlation for each column
  for(co in 1:ncol(select_matrix)){
    idx <- base::which.max(correlation_matrix[, co])
    if(select_matrix[idx, co] == "r"){ # Is it also marked by set in (r)ow as highest? 
      select_matrix[idx, co] <- "b"    # Then it is marked as highest by (b)oth.
    } else{
      select_matrix[idx, co] <- paste0(select_matrix[idx, co], "c") # Marked as highest by column alone.
    }
  }
  
  selection_df <- data.frame(a = 0, b = 0, d = 0, h = 0, k = 0) 
  colnames(selection_df) <- c(name_set_A, name_set_B, "max_corr_for_ME_in_x", "correlation" ,"overlap_of_genes")
  # Add correlation values to those found to be highest.
  for(ro in 1:nrow(correlation_matrix)){
    for(co in 1:ncol(correlation_matrix)){
      if(select_matrix[ro, co] %in% c("b", "c", "r")){
        # Save those that pass the filter
        select_x <- select_matrix[ro, co]
        select_x <- sub("r", name_set_A, select_x) # Names are a little unclear, so we change them
        select_x <- sub("c", name_set_B, select_x)
        select_x <- sub("b", "Both", select_x) 
        
        selection_df <- rbind(selection_df, c(rownames(select_matrix)[ro], 
                                              colnames(select_matrix)[co], 
                                              select_x,
                                              correlation_matrix[ro, co],
                                              overlap_matrix[ro, co]))
        # make a text matrix for plotting
        select_matrix[ro, co] <- paste0(select_matrix[ro, co],"\n", as.character(round(correlation_matrix[ro, co],2)))
      }
    }
  }
  
  selection_df <- selection_df[-1,]
  
  A.n <- set_A$colors %>% table()
  names(A.n) <- paste0("ME", names(A.n))
  A.n <- as.data.frame(A.n)
  colnames(A.n) <- c(name_set_A, paste0("n_", name_set_A))
  B.n <- set_B$colors %>% table()
  names(B.n) <- paste0("ME", names(B.n))
  B.n <- as.data.frame(B.n)
  colnames(B.n) <- c(name_set_B, paste0("n_", name_set_B))

  selection_df <- merge(selection_df, A.n, by = name_set_A)
  selection_df <- merge(selection_df, B.n, by = name_set_B)
  
  selection_df <- selection_df[order(selection_df$correlation, decreasing = T), ]
  
  
  output <- list(overlap = overlap_matrix, 
                 correlation = correlation_matrix, 
                 selection_matrix = select_matrix,
                 selection_df = selection_df)
  
  
  if(plot_true){
    
    pheatmap::pheatmap(overlap_matrix, cluster_cols = F, 
                       cluster_rows = F, 
                       color = colorRampPalette(c("#ffffff","#2171B5"), bias = 2)(10), 
                       display_numbers = select_matrix, 
                       number_color = "#000000", 
                       fontsize_number = 8, 
                       breaks = seq(0,1,0.1), 
                       main = paste0(name_set_A, " is rows\n", name_set_B, " is columns"),
                       silent = T) -> plt
    output[["plt"]] <- cowplot::plot_grid(plt$gtable)  
  }
  
  return(output)
}
