---
title: "Graph TOM Matrix"
author: "Marius Strand"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Load in packages
```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(igraph)
```


Load in module data, taxonomy and TOM matrix
```{r}
# Get the module information
load(file = "../data/module_data/otu_G_rep_0.005_CSS_log2_bicor_noPcC_10000_noPam_FW_and_SW_modules.RData")

# Get the taxonomy from the raw data save file and remove what we do not need
load(file = stage2results$input_file); rm(otu, sample_info.otu)

# Load in the TOM
load(stage2results$modules$TOMFiles)
# The TOM is saved as a dist object and needs to be converted to a matrix
TOM <- as.matrix(TOM)
# Add OTU names to the TOM matrix. It is symmetrical so rownames = colnames
rownames(TOM) <- colnames(TOM) <- names(stage2results$modules$colors)
```

Which taxonomic level should the graph be colored with in addition to modules?
```{r}
selected_taxa <- "Phylum"
```



Convert module labels and taxonomy to hex colors
```{r}
taxonomy_info <- 
  otu_taxonomy %>% 
  rownames_to_column(var = "OTU_name") %>%
  dplyr::select("OTU_name", selected_taxa) %>%
  mutate_all(.funs = list(as.character)) 

module_info <- 
  stage2results$modules$colors %>% 
  as.data.frame() %>%
  rownames_to_column(var = "OTU_name") %>% 
  dplyr::rename(Module = ".")

graph_info <- left_join(taxonomy_info, module_info, by = "OTU_name")

# Converts R-colors to hex colors
color2hex <- function(x){
  x <- col2rgb(x)
  rgb(x[1,], x[2,], x[3,], maxColorValue = 255)
}

# Add specific colors to the taxa
taxa_colors <- 
  graph_info %>% 
  dplyr::select(selected_taxa) %>% 
  unique() %>% 
  mutate(tax_color = colorRampPalette(RColorBrewer::brewer.pal(8, "Accent"))(nrow(.)))

graph_info_colors <- 
  left_join(graph_info, taxa_colors) %>% 
  mutate(module_color = WGCNA::labels2colors(Module)) %>% 
  mutate(module_color = color2hex(module_color)) %>% 
  mutate(module_color = paste0(module_color,70))
```

If all lines are too thick, reduce the strength (between 0 and 1).
The lower the number the weaker the lines.
```{r}
strength_adjust = 1
```


```{r}
g <- graph.adjacency(TOM, mode="undirected", weighted= TRUE)

#~https://stackoverflow.com/questions/28366329/how-to-scale-edge-colors-igraph
igraph::delete.edges(g, which(E(g)$weight <1))
E(g)$width <- E(g)$weight*strength_adjust + min(E(g)$weight)

set.seed(231) # Ensures the same layout given the same data.
l <- layout_with_fr(g, weights = E(g)$weight)


# Order graph_info_colors by the graph 
graph_info_colors <- graph_info_colors[which(graph_info_colors$OTU_name %in% V(g)$name),]

# Ensure that the names are in the same order
if(all(V(g)$name == graph_info_colors$OTU_name)){cat("\nOTU names match")}

# Add square shapes to hub OTUs
V(g)$shape <- ifelse(V(g)$name %in% stage2results$hubs[-1], "square", "circle") #-1 means dont use module 0

# OTUs in modules have larger nodes
V(g)$size <- ifelse(graph_info_colors$Module != 0, 10, 5)

# And larger text
V(g)$label.cex <- ifelse(graph_info_colors$Module != 0, 0.8, 0.4)

# Remove everything but the number to increase readability
V(g)$name =  sub("OTU_", "", graph_info_colors$OTU_name)

```

Find distinct entires for the plot legends
```{r}
module_labels <- 
  graph_info_colors %>% 
  dplyr::select(Module, module_color) %>% 
  distinct() %>% 
  arrange(Module)

tax_labels <- 
  graph_info_colors %>% 
  dplyr::select(Phylum, tax_color) %>% 
  distinct()
```


Plot the graphs, leftmost is colored by module, rightmost is colored by taxonomic classification
```{r fig.height=10, fig.width=15}
par(mfrow = c(1,2))

par(mar = c(0,0,0,0))

plot(g, layout = l, vertex.color = graph_info_colors$module_color)
legend("topleft", legend = paste0("mM", 0:(nrow(module_labels)-1)), fill=module_labels$module_color)

plot(g, layout = l, vertex.color = graph_info_colors$tax_color)
legend("topleft", legend = tax_labels$Phylum, fill=tax_labels$tax_color)

par(mfrow = c(1,1))
```


Save the graphs as a pdf
```{r}

pdf(file = paste0("../images/graphs/TOMgraph_", stage2results$identity ,".pdf"), width = 10, height = 6)

par(mfrow = c(1,2))

par(mar = c(0,0,0,0))

plot(g, layout = l, vertex.color = graph_info_colors$module_color)
legend("topleft", legend = paste0("mM", 0:(nrow(module_labels)-1)), fill=module_labels$module_color)

plot(g, layout = l, vertex.color = graph_info_colors$tax_color)
legend("topleft", legend = tax_labels$Phylum, fill=tax_labels$tax_color)

par(mfrow = c(1,1))

dev.off()
```

